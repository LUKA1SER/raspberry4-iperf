# Durchsatzmessungen mit iPerf3 auf dem Raspberry Pi 4

# Voraussetzungen
Das Tool ist darauf ausgelegt auf dem Raspberry Pi 4 (im folgenden RP4) ausgeführt zu werden, da der RP4 höhere
Bandbreiten als seine Vorgänger unterstützt.

Folgende Software muss auf dem RP4 installiert sein: 
### Python 3
    sudo apt-get install python3
    
Folgende Python-Module müssen installiert sein: 

    datetime
    email 
    ftplib
    getpass
    scapy
    shutil 
    smtplib
    socket 
    tkinter
    os

Sind diese Module nicht installiert (oder nicht aktuell) müssen sie per Pip3 installiert werden. 
    
   
### iPerf3
    sudo apt-get install iperf3
    
### Quagga Linux
- Beim installieren über den Paketmanager ist es bei den Tests häufig zu fehlern gekommen.
Es wird empfohlen Quagga manuell über folgenden [Link](https://packages.debian.org/de/jessie/armhf/quagga/download) 
herunterzuladen. Für den RaspberryPi muss die `armhf` Version gewählt werden.
- Die heruntegeladene `.deb`-Datei kann dann mit `sudo dpkg- i Paketname.deb` installiert werden
- Alternativ installation über apt `sudo apt-get install quagga quagga-docs`

### Prüfen ob Quagga korrekt installiert ist
Wenn Quagga korrekt installiert wurde sollten im Verzeichnis `/etc/quagga/` die Dateien `daemons` und `debian.conf` 
vorhanden sein. 

### Konfiguration von Quagga auf dem RP4
- Damit Quagga starten kann müssen die beiden Konfigurationsdatein `bgpd.conf.example` und `zebra.conf.example` aus dem 
Verzeichnis `/usr/share/doc/quagga/examples/` in das Verzeichnis `/etc/quagga/` kopiert und in `bgpd.conf` bzw. 
`zebra.conf` umbenannt werden. 

- In der Datei `/etc/quagga/daemons` müssen die Parameter `zebra` und `bgpd` auf `yes` geändert werden. 

- Zum starten von Quagga `sudo /etc/init-d/quagga` ausführen

- Sofern kein anderer Port in der Konfiguration festgelegt wurde läuft der BGP-Daemon auf Port `2605`

- Um auf die Konfigurationsschnittstelle zuzugreifen `telnet 127.0.0.1 2605` in die Konsole eingebem.
Quagga kann analog zu einem Cisco-Router konfiguriert werden.

- Das Standardpasswort für Quagga ist `zebra`

#### Beispielkonfiguration
- conf t
- router bgp 65001
- bgp router-id 198.18.178.2
- neighbour 198.18.178.1 remote-as 65001
- network 198.1.1.1/32
- Speichern mit `copy running-config startup-config`
    
## Inhalte dieses Repositories
Das Repository enthält den Hauptordner
> iperf

sowie die Nebensächlichen Ordner
> configs ; scripts

- Im Ordner configs befinden sich Dateien, in denen die Konfiguration des RP4 und der zum Test verwendeten Router gespeichert ist. 
Diese Konfigurationen sind nur bedingt korrekt und nicht in jedem Punkt aktuell. 
- Im Ordner scripts befinden sich Teilskripte, welche zum Test geschrieben wurden.
- Der Ordner iperf enthält das eigentliche Skript. Die Hauptdatei ist 

> main.py
      
und kann durch folgenden Befehl gestartet werden (root Rechte sind dabei unbedingt erforderlich!)

      sudo python3 main.py
      
Alle anderen Dateien im Ordner werden automatisch vom Skript verwendet und müssen nicht von Hand gestartet werden. 
      
## Nutzungsanleitung und Funktionen
- Um die GUI zu starten die Datei `main.py` im Ordner `iperf` mit root-Rechten starten: `sudo python3 main.py`

![alt text](https://gitlab.com/LUKA1SER/ihk-produktion-raspberry/-/raw/develop/gui.png)

|Feld | Funktion | 
|------|----------|
|Hilfe|Öffnet ein Fenster mit Nutzungsanleitung|
|Einstellungen|Öffnet das Eintellungsfenster; Hier kann die IP-Adr. des RP4 und das Standardgateway geändert werden|
|IP-Adresse des Messservers | Eintragen der IP-Adr. der iPerf3 Gegenstelle (muss als Server registriert sein) |
|Auswahfeld für IP-Adresse | Vorauswahl für Messserver |
|IP-Adresse des Routers | IP-Adresse des angeschl. Routers um ggf. SNMP-Walk durchzuführen |
| Protokoll | TCP/UDP für die iPerf3 Messung verwenden |
|Auswahlfeld UDP | Paketgröße für die UDP Pakete wählen |
|SNMP-Walk|Haken setzen, wenn SNMP-Walk durchgeführt werden soll. Es muss dabei eine IP im 2. Feld eingetragen sein|
|Community|Auswahl der Community für den SNMP-Walk|
|E-Mail|Eintragen der E-Mail-Adresse an die das Ergebnis gesendet werden soll|
|Passwort|Eintragen des Passwortes für den E-Mail Account, von dem die E-Mail versendet wird|
|FTP|Eintragen von Adresse, User, Passwort für den FTP Upload|
|Auswahlfeld FTP| Vorauswahl für FTP-Server|
|Messung starten|Messung wird gestartet. Im Hintergrund wird auf der Konsole er aktuelle Status angegeben|




## Support
-  Autor/Entwickler: Lukas Kaiser
- letztes Änderungsdatum: 07.01.2021

Ansprechpartner bei Problemen, Bugs, Fragen oder Anregungen: 
- Lukas Kaiser (lukas.kaiser@ewe.de)
- Lars Jungclaus (lars.jungclaus@ewe.de)