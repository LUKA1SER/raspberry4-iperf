
import os

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))

f = os.popen("ifconfig eth0 | grep 'inet' | awk 'NR==1{print $4}'")
my_ip = f.read()

print(s.getsockname()[0] + "/" + my_ip)
s.close()