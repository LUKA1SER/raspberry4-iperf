from tempfile import mkstemp
from shutil import move, copymode
from os import fdopen, remove
import re

ip_pattern = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)')

def change_pi_static_ip(new_ip_address, new_gateway):
    file_path = 'dhcpcd.conf'
    # Temporäre Datei erstellen
    fh, abs_path = mkstemp()
    with fdopen(fh, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                line = re.sub(
                    r'([1-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])(\.(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){3}\/\d+',
                    new_ip_address, line.rstrip())
                new_file.write(line + "\n")
    # Berechtigungen von der alter zur neuen Datei kopieren
    copymode(file_path, abs_path)
    # Originale Datei loeschen
    remove(file_path)
    # Neue Datei verschieben
    move(abs_path, file_path)

    file_path = 'dhcpcd.conf'
    # Temporäre Datei erstellen
    fh, abs_path = mkstemp()
    with fdopen(fh, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                line = re.sub(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)', new_gateway, line.rstrip())
                new_file.write(line + "\n")
    # Berechtigungen von der alter zur neuen Datei kopieren
    copymode(file_path, abs_path)
    # Originale Datei loeschen
    remove(file_path)
    # Neue Datei verschieben
    move(abs_path, file_path)

def change_pi_gateway(new_gateway):
    file_path = 'dhcpcd.conf'
    # Temporäre Datei erstellen
    fh, abs_path = mkstemp()
    with fdopen(fh, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                line = re.sub(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)', new_gateway, line.rstrip())
                new_file.write(line + "\n")
    # Berechtigungen von der alter zur neuen Datei kopieren
    copymode(file_path, abs_path)
    # Originale Datei loeschen
    remove(file_path)
    # Neue Datei verschieben
    move(abs_path, file_path)

change_pi_static_ip('2.2.2.2/2', '1.1.1.1')
