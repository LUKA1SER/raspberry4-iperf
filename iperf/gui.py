from shutil import move, copymode
from tkinter import *
from tkinter import filedialog
from datetime import datetime
from tempfile import mkstemp
from os import fdopen, remove
import measurement
import sendmail
import ftp_upload
import socket
import os
import re

def change_pi_configuration(new_ip_address, new_gateway):
    file_path = '/etc/dhcpcd.conf'
    # Temporäre Datei erstellen
    fh, abs_path = mkstemp()
    with fdopen(fh, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                line = re.sub(
                    r'([1-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])(\.(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){3}\/\d+',
                    new_ip_address, line.rstrip())
                new_file.write(line + "\n")
    # Berechtigungen von der alter zur neuen Datei kopieren
    copymode(file_path, abs_path)
    # Originale Datei loeschen
    remove(file_path)
    # Neue Datei verschieben
    move(abs_path, file_path)

    # Datei erneut öffnen und Gateway aendern

    file_path = '/etc/dhcpcd.conf'
    # Temporäre Datei erstellen
    fh, abs_path = mkstemp()
    with fdopen(fh, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                line = re.sub(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$)', new_gateway, line.rstrip())
                new_file.write(line + "\n")
    # Berechtigungen von der alter zur neuen Datei kopieren
    copymode(file_path, abs_path)
    # Originale Datei loeschen
    remove(file_path)
    # Neue Datei verschieben
    move(abs_path, file_path)

    os.system('sudo reboot')


class Gui:
    def __init__(self):
        self.draw_window()
        self.chosen_email = ''
        self.email_password = ''
        self.server_address = ''
        self.router_address = ''
        self.chosen_protocol = ''
        self.snmp_choice = False
        self.snmp_community = ''
        self.chosen_package_size = 0
        self.ftp_server = ''
        self.ftp_username = ''
        self.ftp_password = ''
        self.date = ''

    def draw_window(self):
        window = Tk()
        window.title("IPerf Messung")
        window.geometry('1000x1000')
        self.draw_contents(window)
        window.mainloop()

    def open_help_window(self):
        help_window = Toplevel()
        help_window.geometry('750x600')
        help_window.title("Bedienungsanleitung")
        help = Label(help_window, text='''
             - - - Gebrauchsanweisung - - -
             1. IP-Adresse des Messervers und des angeschlossenen Routers eintragen (nur wenn SNMP-Walk gemacht wird)
             2. Auswählen ob TCP oder UDP genutzt werden soll; Bei UDP ebenfalls Paketgröße auswählen
             3. Haken setzen, wenn SNMP-Walk durchgeführt werden soll und Community des Routers wählen.
             4. In das E-Mail Feld kann eine E-Mail eingetragen werden, an welche die Ergebnisse versendet werden.
                Wird das Feld freigelassen wird keine E-Mail versendet. 
             5. In das Passwort-Feld musst das Passwort für die EWE E-Mail eingetragen werden, von welcher die 
                E-Mail versendet wird.
                Soll keine E-Mail versendet werden kann auch dieses Feld freigelassen werden.
             6. FTP-Server, User und Passwort eingeben, wenn Ergebnisdateien ebenfalls hochgeladen werden sollen. 
             7. Mit einem Klick auf "Messung starten" werden die Messungen gestartet.
             8. Sind alle Messungen beendet wird das Ergebnis der iPerf Messung in einem neuen Fenster angezeigt.
                Die Ergebnisse werden ebenfalls im Ordner /results/ mit Zeitstempel gepspeichert. 
             9. Klick auf "Einstellungen" öffnet das Eintsllungsfenster. Hier kann die IP/Standardgateway des Raspberrys
                geändert werden. 
             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
             Fragen, Bugs und Verbesserungsvorschläge bitte an: lukas.kaiser@ewe.de oder lars.jungclaus@ewe.de
                
        ''').pack()

    def open_config_window(self):
        config_window = Toplevel()
        config_window.geometry('600x500')
        config_window.title("Konfiguration des Raspberry-Pi")

        raspberry_ip_label = Label(config_window, text="IP-Addresse des Raspberry-Pi (mit CIDR Subnetzmaske)")
        raspberry_ip_input = Entry(config_window, borderwidth=1, width=15)

        gateway_label = Label(config_window, text="Standardgateway")
        gatetway_input = Entry(config_window, borderwidth=1, width=15)

        info_label = Label(config_window, text='''Achtung: Nach Klick auf "Anwenden" wird der Raspberry
                                                neugestartet und das Skript muss nach Neustart erneut 
                                                per Hand gestartet werden.''')

        apply_button = Button(config_window, bg='orange', fg='black', text="Anwenden",
               command=lambda: [
                   change_pi_configuration(raspberry_ip_input.get(), gatetway_input.get())])

        raspberry_ip_label.place(x=0, y=0)
        raspberry_ip_input.place(x=0, y=20)

        gateway_label.place(x=0, y=50)
        gatetway_input.place(x=0, y=70)

        apply_button.place(x=0, y=100)
        info_label.place(x=0, y=150)

    def draw_contents(self, window):
        server_address_label = Label(window, text="IP-Adresse des Messservers:", font='Helvetica 14 bold')
        server_address_input = Entry(window, borderwidth=1, width=15)

        server_preselection = StringVar()
        server_preselection.set('---')
        server_preselection_menu = OptionMenu(window, server_preselection, '85.16.114.241')

        router_address_label = Label(window, text="IP-Adresse des Routers:", font='Helvetica 14 bold')
        router_address_field = Entry(window, borderwidth=1, width=15)

        protocol_label = Label(window, text="Welches Protokoll soll verwendet werden?", font='Helvetica 14 bold')
        tcp_label = Label(window, text="TCP", font='Helvetica 14 bold')
        udp_label = Label(window, text="UDP", font='Helvetica 14 bold')

        chosen_protocol = StringVar()
        tcp_button = Radiobutton(window, variable=chosen_protocol, value="tcp")
        udp_button = Radiobutton(window, variable=chosen_protocol, value="udp")

        do_snmp_walk = BooleanVar()
        snmp_checkbox_label = Label(window, text="SNMP-Walk", font='Helvetica 14 bold')
        snmp_checkbox = Checkbutton(window, variable=do_snmp_walk)

        snmp_community = StringVar()
        snmp_community.set('---')
        snmp_community_label = Label(window, text="Community", font='Helvetica 14 bold')
        snmp_community_menu = OptionMenu(window, snmp_community, '86cce89f', 'MCE-SNMP-VFyVewap', 'KelloggsClusters',
                                         '555')

        package_size = IntVar()
        package_size.set(0)
        package_size_menu = OptionMenu(window, package_size, 64, 128, 256, 512, 1024,
                                       1300, 1500)

        email_label = Label(window, text="E-Mail:", font='Helvetica 14 bold')
        email_input = Entry(window, borderwidth=1, width=15)

        email_password_input_label = Label(window, text="Passwort: ", font='Helvetica 14 bold')
        email_password_input = Entry(window, show="*", width=15)

        ftp_server_label = Label(window, text="FTP-Server", font='Helvetica 14 bold')
        ftp_username_label = Label(window, text='FTP-Username', font='Helvetica 14 bold')
        ftp_password_label = Label(window, text='FTP-Passwort', font='Helvetica 14 bold')

        ftp_server_input = Entry(window, width=15)
        ftp_username_input = Entry(window, width=15)
        ftp_password_input = Entry(window, show="*", width=15)

        ftp_preselection = StringVar()
        ftp_preselection.set('---')
        ftp_preselection_menu = OptionMenu(window, ftp_preselection, '85.16.114.241')

        help_button = Button(window, bg='yellow', text="Hilfe", command=self.open_help_window)

        config_button = Button(window, bg='gray69', text="Einstellungen", command=self.open_config_window)

        f = os.popen("ifconfig eth0 | grep 'inet' | awk 'NR==1{print $2,$4}'")
        my_ip = f.read()

        ip_label = Label(window, text="Aktuelle IP-Adresse:" + "\n" + my_ip)

        # Anordnung der Felder
        server_address_input.place(x=260, y=0)
        server_address_label.place(x=0, y=0)

        server_preselection_menu.place(x=385, y=0)

        router_address_field.place(x=260, y=35)
        router_address_label.place(x=0, y=35)

        protocol_label.place(x=0, y=65)
        tcp_label.place(x=50, y=90)
        tcp_button.place(x=100, y=90)
        udp_label.place(x=50, y=110)
        udp_button.place(x=100, y=110)
        package_size_menu.place(x=150, y=110)

        snmp_checkbox_label.place(x=0, y=140)
        snmp_checkbox.place(x=125, y=140)
        snmp_community_label.place(x=160, y=140)
        snmp_community_menu.place(x=270, y=135)

        email_label.place(x=0, y=170)
        email_input.place(x=160, y=170)

        email_password_input_label.place(x=0, y=200)
        email_password_input.place(x=160, y=200)

        ftp_server_label.place(x=350, y=170)
        ftp_username_label.place(x=350, y=200)
        ftp_password_label.place(x=350, y=230)

        ftp_server_input.place(x=500, y=170)
        ftp_username_input.place(x=500, y=200)
        ftp_password_input.place(x=500, y=230)

        ftp_preselection_menu.place(x=625, y=165)

        help_button.place(x=600, y=0)

        config_button.place(x=600, y=30)

        submit_button = Button(window, bg='blue', fg='white', text="Messung starten",
                               command=lambda: [self.get_inputs(server_address_input.get(), server_preselection.get(), router_address_field.get(), email_input.get(),
                                                                email_password_input.get(), chosen_protocol.get(),
                                                                do_snmp_walk.get(), snmp_community.get(),
                                                                package_size.get(), ftp_server_input.get(), ftp_preselection.get(),
                                                ftp_username_input.get(), ftp_password_input.get()),
                                                self.start_measurement(window)])
        submit_button.place(x=200, y=260)

        ip_label.place(x=0, y=300)

    # Sammelt alle eingetragenen Werte
    def get_inputs(self, server_address_input, server_preselection, router_address_input, email_input, email_password, chosen_protocol, snmp_choice, snmp_community,
                   package_size, ftp_server, ftp_preselection, ftp_username, ftp_password):
        if not server_address_input and server_preselection != '---':
            self.server_address = server_preselection
        else:
            self.server_address = server_address_input

        self.router_address = router_address_input
        self.chosen_email = email_input
        self.email_password = email_password
        self.chosen_protocol = chosen_protocol
        self.snmp_choice = snmp_choice
        self.snmp_community = snmp_community
        self.chosen_package_size = package_size
        self.ftp_server = ftp_server
        self.ftp_username = ftp_username
        self.ftp_password = ftp_password

        if not ftp_server and ftp_preselection != '---':
            self.ftp_server = ftp_preselection
        else:
            self.ftp_server = ftp_server

    # Wird nach dem Klicken des Buttons "Messung starten" aufgerufen
    def start_measurement(self, window):
        self.date = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
        iperf = measurement.Measurement(self.server_address, self.router_address, self.chosen_protocol, self.snmp_choice,
                                        self.snmp_community, self.chosen_package_size,
                                        self.chosen_email, self.date)
        print(self.date)
        iperf.initialize_measurement()

        if self.chosen_email != "":
            self.mail_sender_init(self.chosen_email, self.email_password)

        if self.ftp_server != "":
            self.ftp_init(self.ftp_server, self.ftp_username, self.ftp_password)


        self.open_results(window)

    def open_results(self, window):
        iperf_filename_cs = 'results/' + self.date + '_iperf-client-server-result.txt'
        iperf_filename_sc = 'results/' + self.date + '_iperf-server-client-result.txt'
        with open(iperf_filename_cs, 'r') as file:
            result_text_cs = file.read()

        with open(iperf_filename_sc, 'r') as file:
            result_text_sc = file.read()

        print("Ergebnisse der Client-Server-Messung: " + "\n")
        print(result_text_cs + "\n")
        print("Ergebnisse der Server-Client-Messung: " + "\n")
        print(result_text_sc)

        text_field = Text(window)
        text_field.grid(row=0, column=0)
        text_field.insert(END,"Ergebnisse der Client-Server-Messung: " + result_text_cs + "\n" +
                          'Ergebnisse der Server-Client-Messung: ' + '\n' + result_text_sc +
                          '\n Das Skript wird mit schließen dieses Fensters beendet.')

    # Initialisiert die Klasse MailSender und ruft die Methonde send_mail() auf, welche die E-Mail verschickt
    def mail_sender_init(self, chosen_email, password):
        mail_sender = sendmail.MailSender(self.chosen_email, password, self.date, self.snmp_choice)

        if mail_sender.check_connectivity:
            print("Mail wird versendet...")
            mail_sender.send_email()
        else:
            print("E-Mail kann nicht versendet werden, bitte stelle eine Verbindung zum Internet her.")
            return

    def ftp_init(self, ftp_server, ftp_username, ftp_password):
        print("Datei wird hochgeladen \n "
              "Server: " + ftp_server)
        uploader = ftp_upload.FTP_Uploader(ftp_server, ftp_username, ftp_password, self.date, self.snmp_choice)

        uploader.ftp_upload()
        print("Datei hochgeladen")
