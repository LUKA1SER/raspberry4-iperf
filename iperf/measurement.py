import os


class Measurement:
    def __init__(self, server_address, router_address, protocol, snmp_choice, snmp_community, package_size, email, date):
        self.server_address = server_address
        self.router_address = router_address
        self.protocol = protocol
        self.package_size = package_size
        self.snmp_choice = snmp_choice
        self.snmp_community = snmp_community
        self.email = email
        self.date = date

    def initialize_measurement(self):
        if self.protocol == 'tcp':
            self.tcp_measurement()
        elif self.protocol == 'udp':
            self.udp_measurement()

        if self.snmp_choice:
            self.snmp_walk()
        else:
            return

    # IPerf TCP Messung durchfuehren
    def tcp_measurement(self):
        print("Gewählte IP: " + self.server_address)
        print("Test wird durchgefuehrt")
        os.system('iperf3 -c ' + self.server_address + ' > results/' + self.date + '_iperf-client-server-result.txt')
        print("Test 1/2 abgeschlossen")
        os.system('iperf3 -c ' + self.server_address + ' -R > results/' + self.date + '_iperf-server-client-result.txt')
        print("Test 2/2 abgeschlossen")

        print("MTU-Messung wird gestartet")
        os.system('sudo tcpdump -n icmp -c 1472 > results/' + self.date + '_mtu_tcpdump_result.txt & sudo python3 mtu.py ' + self.server_address)
        print("MTU-Messung abgeschlossen")

    # IPerf UDP Messung mit gewaehlter Paketgroesse durchfuehren
    def udp_measurement(self):
        print("Gewählte IP: " + self.server_address)
        print("Gewählte Paketgröße: " + str(self.package_size))
        print("Test wird durchgefuehrt")
        os.system('iperf3 -c ' + self.server_address + ' -u -l + ' + str(self.package_size) + ' -b 0  > results/' + self.date + '_iperf-client-server-result.txt')
        print("Test 1/2 abgeschlossen")
        os.system('iperf3 -c ' + self.server_address + ' -u -l + ' + str(self.package_size) + ' -b 0 -R > results/' + self.date + '_iperf-client-server-result.txt')
        print("Test 2/2 abgeschlossen")

        print("MTU-Messung wird gestartet")
        os.system('sudo tcpdump -n icmp -c 1472 > results/' + self.date + '_mtu_tcpdump_result.txt & sudo python3 mtu.py ' + self.server_address)
        print("MTU-Messung abgeschlossen")

    def snmp_walk(self):
        print("SNMP-Walk wird durchgeführt")
        os.system('snmpwalk -v2c -c ' + self.snmp_community + ' ' + self.router_address + ' > results/' + self.date + '_snmp-result.txt')
        print("SNMP-Walk abgeschlossen.")

