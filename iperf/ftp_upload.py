from ftplib import FTP
import os

class FTP_Uploader:
    def __init__(self, ftp_server, ftp_username, ftp_password, date, snmp_choice):
        self.server = ftp_server
        self.username = ftp_username
        self.password = ftp_password
        self.date = date
        self.snmp_choice = snmp_choice
        print(self.date)
        self.iperf_file = self.date + '_iperf-result.txt'
        self.snmp_file = self.date + '_snmp-result.txt'


    def ftp_upload(self):
        ftp = FTP(self.server)
        ftp.login(self.username, self.password)

        fp = open(self.iperf_file, 'rb')
        ftp.storbinary('STOR %s' % os.path.basename(self.iperf_file), fp, 1024)
        fp.close()

        if self.snmp_choice:
            fp = open(self.snmp_file, 'rb')
            ftp.storbinary('STOR %s' % os.path.basename(self.snmp_file), fp, 1024)
            fp.close()

        ftp.quit()
        print('Files uploaded')
