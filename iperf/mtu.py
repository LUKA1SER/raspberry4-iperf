from scapy.all import IP, ICMP, send
import sys

# Payload Größe zu Beginn = 2 Byte
payload = '  '

# Verschickt Packet mit payload von 2 Byte und ehöhrt payload dann um 2 Byte
# Pakete werden so lange verschickt, bis payload 1500 Byte groß ist
while len(payload) <= 1472:
    packet = IP(dst=sys.argv[1])/ICMP()/payload
    print(len(packet))
    send(packet)
    payload += '  '


print("Alle Pakete gesendet")

