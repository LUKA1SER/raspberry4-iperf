import smtplib
import os
from getpass import getpass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from datetime import datetime

class MailSender:
    def __init__(self, recipient_email, password, date, snmp_choice):
        self.port = 465
        self.smtp_server = "smtp.gmail.com"
        self.sender_email = "lukaiser.produktiontest@gmail.com"
        self.receiver_email = recipient_email
        self.passwod = password
        self.date = date
        self.snmp_choice = snmp_choice
        print(self.date)

    def send_email(self):
        msg = MIMEMultipart()
        msg['From'] = self.sender_email
        msg['To'] = self.receiver_email
        msg['Subject'] = "Ergebnisse der Messung vom " + self.date

        body = "Die Ergebnisse der Messung vom " + self.date + " finden sie in den Dateien im Anhang"
        msg.attach(MIMEText(body, 'plain'))

        iperf_client_server_filename = 'results/' + self.date + '_iperf-client-server-result.txt'
        iperf_server_client_filename = 'results/' + self.date + '_iperf-server-client-result.txt'
        snmp_filename = 'results/' + self.date + '_snmp-result.txt'
        mtu_filename = 'results/' + self.date + '_mtu_tcpdump_result.txt'

        if self.snmp_choice:
            attachments = [iperf_client_server_filename, iperf_server_client_filename, mtu_filename, snmp_filename]
        else:
            attachments = [iperf_client_server_filename, iperf_server_client_filename, mtu_filename]

        for filename in attachments:
            f = filename
            part = MIMEBase('applicaition', "octet-stream")
            part.set_payload(open(f, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename= ' + f)
            msg.attach(part)

        text = msg.as_string()

        try:
            with smtplib.SMTP_SSL(self.smtp_server, self.port) as server:
                server.login(self.sender_email, self.passwod)
                server.sendmail(self.sender_email, self.receiver_email, text)
                server.quit()
            print("E-Mail versendet ✅")
        except smtplib.SMTPAuthenticationError:
            print("Es wurde ein falsches Passwort eingegeben!")

    # Prüft durch ping an 8.8.8.8 ob eine Verbindung zum Internet besteht
    def check_connectivity(self):
        response = os.system("ping 8.8.8.8 -c 4")

        if response == 0:
            print("Internetverbindung ✅")
            return True
        else:
            print("Keine Internetverbindung ❌")
            return False
